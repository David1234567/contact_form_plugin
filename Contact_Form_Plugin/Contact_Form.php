<?php
/**
 * @package contact_formulier
 * @version 1
 */
/*
Plugin Name: Contact Formulier
Plugin URI: http://wordpress.org/
Description: Contact Formulier backend opdracht appsaloon
Author: David Singh
Version: 2
*/

namespace ContactForm;


//autoloader
require_once('Autoloader.php');

//models
require_once('Form.php');
require_once('Contact.php');
require_once('Map.php');

//controllers
require_once('Custom_Post_Type_Controller.php');
require_once('Form_Controller.php');
require_once('Data_Export_Controller.php');
require_once('Map_Controller.php');


class Contact_Form {
    public function __construct() {
        $this->run();
    }

    function run() {
        new Custom_Post_Type_Controller();
        new Form_Controller();
        new Data_Export_Controller();
        new Map_Controller();
    }
}

new Contact_Form();

?>