<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 16:49
 */

namespace ContactForm;


class Custom_Post_Type_Controller
{

    public function __construct()
    {
        //initializes custom post type on init hook
        add_action('init', array($this, 'custom_post_type_init'));
        //redefines action and filter to display meta-tag names and content as columns for custom post type
        add_action( 'manage_contact_info_posts_custom_column' , array($this, 'set_custom_contact_info_columns'), 10, 2 );
        add_filter( 'manage_contact_info_posts_columns', array($this, 'set_custom_edit_contact_info_columns') );
    }

    /**
     *  defines and registers custom post type
     *  $args:
     *      public: do not make posts of this type public
     *      show_ui: show custom post type in admin panel
     *      supports: can set a title and custom fields when creating new entry in admin panel
     *      labels: descriptive names for each functionality in the admin panel for this custom post type
     *
     */
    function custom_post_type_init() {
        $args = array(
            'public' => false,
            'show_ui' => true,
            'description' => 'Info about contacts',
            'supports' => array('title', 'custom-fields'),
            'labels' => array(
                'name' => 'contacts',
                'singular_name' => 'contact',
                'add_new' => 'add new contact',
                'add_new_item' => 'add new contact',
                'edit_item' => 'edit contact',
                'new_item' => 'new contact',
                'view_item' => 'view contact',
                'search_items' => 'search contacts',
                'not_found' => 'no contacts found',
                'not_found_in_trash' => 'no contacts found in trash'
            )
        );

        //registers custom post type "contact_info" with the above arguments
        register_post_type('contact_info', $args);
    }

    /** FILTER:
     *  sets the names of all custom fields in the admin panel
     */
    function set_custom_edit_contact_info_columns($columns) {
        $columns['adres_straat'] =  'adres_straat' ;
        $columns['adres_num'] =  'adres_num' ;
        $columns['adres_num_postbus'] =  'adres_num_postbus' ;
        $columns['adres_postcode'] =  'adres_postcode' ;
        $columns['adres_gemeente'] =  'adres_gemeente' ;
        $columns['email'] =  'email' ;
        $columns['geslacht'] =  'geslacht' ;
        $columns['geboortedatum'] =  'geboortedatum' ;
        $columns['latitude'] =  'latitude' ;
        $columns['longitude'] =  'longitude' ;
        return $columns;
    }


    /** ACTION:
     * sets the content of all custom fields for each entry of custom post type "contact_info"
     */
    function set_custom_contact_info_columns( $column, $post_id )
    {
        switch ($column) {

            case 'adres_straat' :
            case 'adres_num' :
            case 'adres_num_postbus' :
            case 'adres_postcode' :
            case 'adres_gemeente' :
            case 'email' :
            case 'geslacht' :
            case 'geboortedatum' :
            case 'latitude' :
            case 'longitude' :
                echo get_post_meta($post_id, $column, true);
                break;
        }
    }

}