<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 9:35
 */


namespace ContactForm;

class Form_Controller
{

    private $errors;

    private $contact_added;

    public function __construct()
    {
        add_shortcode('show_contact_form_shortcode', array($this, 'show_contact_form_shortcode'));
        add_action('init', array($this, 'complete'));

    }


    public function show_contact_form_shortcode()
    {
        ob_start();

        $form = new Form();
        if (!count($_POST) > 0 || isset($this->errors)) {

            //display every error message
            foreach ($this->errors??[] as $error) {
                echo $error;
            }
            $form->build();
        }

        return ob_get_clean();

    }

    public function complete()
    {
        ob_start();
        if (count($_POST) > 0 && isset($_POST['adres_straat']) && isset($_POST['adres_num']) && isset($_POST['adres_postcode']) && isset($_POST['adres_gemeente']) && isset($_POST['geslacht']) && isset($_POST['email']) && isset($_POST['geboortedatum'])) {


            $form = new Form();

            $form_data = [
                'adres_straat' => $_POST['adres_straat'],
                'adres_num' => $_POST['adres_num'],
                'adres_num_postbus' => $_POST['adres_num_postbus'] ?? '',
                'adres_postcode' => $_POST['adres_postcode'],
                'adres_gemeente' => $_POST['adres_gemeente'],
                'email' => $_POST['email'],
                'geslacht' => $_POST['geslacht'],
                'geboortedatum' => \DateTime::createFromFormat("d-m-Y", $_POST['geboortedatum'])
            ];

            $validation_response = $form->validate($form_data);

            if ($validation_response['valid'] === true) {

                $contact = new Contact($form_data['email']);
                $contact->setAdresStraat($form_data['adres_straat']);
                $contact->setAdresNum($form_data['adres_num']);
                $contact->setAdresNumPostbus($form_data['adres_num_postbus']);
                $contact->setAdresPostcode($form_data['adres_postcode']);
                $contact->setAdresGemeente($form_data['adres_gemeente']);
                $contact->setGeboortedatum($form_data['geboortedatum']->format("d-m-Y"));
                $contact->setGeslacht($form_data['geslacht']);

                $geolocation = $contact->getGeoLocation($contact);
                if( count($geolocation)>0 && isset($geolocation['latitude']) && isset($geolocation['longitude']) ) {
                    $contact->setLatitude($geolocation['latitude']);
                    $contact->setlongitude($geolocation['longitude']);
                }

                $this->contact_added = $contact->add($contact);


                //add message for success or failure, if contact was added
                add_action('the_content', function () {
                    if($this->contact_added) {
                        echo '<div><h2>Uw informatie werd toegevoegd!</h2><a href="./">terug</a></div>';
                    } else {
                        echo '<div><h2>Er liep iets mis.</h2><a href="./">terug</a></div>';
                    }
                });

            } /// form submission is invalid: response holds error messages
            else {
                $this->errors = $validation_response['errors'];
            }

            echo ob_get_clean();


        }
    }

}