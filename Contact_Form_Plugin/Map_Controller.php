<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 9:36
 */


namespace ContactForm;

class Map_Controller
{
    public function __construct()
    {
        /* adds shortcode to view the map with all data in custom post type "contact_info" by
        *  calling [show_map_shortcode] in a wordpress page
        *  adds action to enqueue google maps api js files in page head
        */

        add_shortcode('show_map_shortcode', array($this, 'show_map_shortcode'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_google_maps_api'));


        /* Defines the google maps API key as a constant
         *  used in:
         *      googlemaps.php
         *      Contact.php class
         *  Defines the google maps API script source as a constant
         *  used in:
         *      googlemaps.php  enqueue scripts function
         */
        define('google_maps_API_key', 'AIzaSyCetaabtVQp7UxKlIGRnn6-ArTvGYeClnY');
        define('google_maps_API_source_script', 'https://maps.google.com/maps/api/js?key=' . google_maps_API_key);
    }


    /**
     * starts output buffering, shows the map HTML and builds an inline javascript block with the contact_info data formatted as a js object
     */
    function show_map_shortcode() {
        ob_start();
        $map = new Map();
        $mapHTML = $map->build_HTML();
        $contactsData = Contact::export(true)['data'];
        $mapJS = $map->build_JS($contactsData);

        echo $mapHTML;
        echo $mapJS;
        return ob_get_clean();
    }



    /**
     *  inserts external google maps API script into page head
     *  inserts script file to render markers and infoboxes in the page head
     */
    function enqueue_scripts_google_maps_api() {
        //determining the url path of the plugin and finding the correct js file
        $local_map_script = plugin_dir_url(__FILE__).'js/embed_google_maps.js';

        //includes custom script in the page head to display markers and infoboxes
        wp_enqueue_script('embed_google_maps', $local_map_script);
        //includes google maps API main script in the page head
        wp_enqueue_script('maps_api_source', google_maps_API_source_script);
    }

}