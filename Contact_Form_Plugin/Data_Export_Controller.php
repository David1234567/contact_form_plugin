<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 16:38
 */

namespace ContactForm;


class Data_Export_Controller
{

    public function __construct()
    {
        //get_export_request should happen before page headers are sent
        //as this file will write its own headers when requested
        add_action( 'init', array($this, 'get_export_request') );
    }

    /**
     *  Users can request a download link by adding the get parameter "export"
     */
    function get_export_request() {
        if(isset($_GET['export'])) {
            Contact::export_to_CSV();
        }
    }

}