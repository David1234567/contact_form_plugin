<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 17:12
 */

namespace ContactForm;


class Map
{

    /**
     * shows the map HTML
     * builds an inline javascript block with the contact_info data formatted as a js array of js objects
     * calls initMap(), a function to populate the map with markers and infoboxes, defined in /js/embed_google_maps.js
     */
    function build_HTML():String
    {
        //the div wrapper that will hold the google maps & markers after initMap()
        $html = '<div id="map" style="height: 380px; width: 550px;">&nbsp;</div>';
        return $html;
    }


    /**
     * @param $contacts_array exported array by Contact class
     * @return String javascript block including a variable 'contacts' JS object with all the contact info provided by this function's argument
     */
    function build_JS($contacts_array):String
    {

        //custom, dynamic inline javascript block, holds all the contact_info data as a js array of js objects
        $JS_block = '<script type="text/javascript">
            var contacts = [
            ';
            foreach($contacts_array as $key=>$contact_info)
            {
            //dont include contacts whose address google geolocation API could not detect, skip to next contact
            if($contact_info['latitude'] == null || $contact_info['longitude'] == null)
                continue;

                //combine all the address values into one string
                $volledig_adres = $contact_info['adres_straat'] .' '.$contact_info['adres_num'].'-'.$contact_info['adres_num_postbus'];
                $volledig_adres .= ' ,'. $contact_info['adres_postcode'].' '.$contact_info['adres_gemeente'];

                //outputs a comma after every object in the array, except for the last one
                $comma = ($key<count($contacts_array)-1)? ',': null;

                //echo the contact data as a js object
                //lat and lng will be used to determine where the marker is placed
                //everything else will be put into the info box for each marker
                $JS_block.= '{
                    lat: '.$contact_info['latitude'].',
                    lng: '.$contact_info['longitude'].',
                    title: "'.$contact_info['email'].'",
                    content: ["'.$volledig_adres.', '.$contact_info['geslacht'].', '.$contact_info['geboortedatum'].'"]
                  }'.$comma;
            }
        $JS_block.= '];';

        //call the js function initMap(), once the js array "contacts" is outputted
        //initMap() is a function in /js/embed_google_maps.js and initializes the map, places the markers and infoboxes
        $JS_block.= ' initMap(); 
               </script>';

        return $JS_block;
    }
}