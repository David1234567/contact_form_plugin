<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 12/1/2017
 * Time: 13:23
 */

namespace ContactForm;


Class Autoloader{

    /**
     * Registreren van autoloader
     */
    public function __construct()
    {
        spl_autoload_register( array( $this, 'kla_core_autoload' ) );
    }

    /**
     * Inladen van PHP classes
     *
     * @param $class
     */
    public function kla_core_autoload( $class ) {
        if ( strpos( $class, 'klasse\\' ) === 0 ) {
            $path = substr( $class, strlen( 'klasse\\' ) );
            $path = strtolower( $path );
            $path = str_replace( '_', '-', $path );
            $path = str_replace( '\\', DIRECTORY_SEPARATOR, $path ) . '.php';

            // core class of niet
            if ( strpos( $class, 'klasse_core') !== false ) {
                $dir = str_replace( '/klasse-core', '', CORE_DIR );
            } else {
                $dir = str_replace( 'mu-plugins/klasse-core', 'plugins', CORE_DIR );
            }

            $path = $dir . DIRECTORY_SEPARATOR . $path;

            if ( file_exists( $path ) ) {
                include $path;
            }
        }
    }
}

new Autoloader();