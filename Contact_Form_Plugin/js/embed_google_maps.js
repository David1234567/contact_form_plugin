//the map var which will refer to the #map div
var map;

//initializes the map variable, creates a new instance of Maps object, as defined in google maps API main script, linked externally
//reads data from the contacts var, created dynamically with PHP at googlemaps.php
//places a marker and infobox for each entry in the contacts var
function initMap() {
    window.onload = function() {
        //creates Map object in div#map, initializes map var
        map = new google.maps.Map(document.getElementById('map'), {
            //default zoom level
            zoom: 8,
            //center on Bampslaan, Hasselt
            center: new google.maps.LatLng(50.9305047, 5.329726099999999),
            //type of map
            mapTypeId: 'roadmap'
        });

        //loop through all entries in contacts array
        contacts.forEach(
            function(contact) {
                //contact.lat: latitude as string
                //contact.lng: longitude as string]
                //contact.title: contact's email
                //contact.content: [volledig adres, geslacht, geboortedatum]

                //creates instance of Marker; using the values lat and lng provided in contacts array
                var marker = new google.maps.Marker({
                    position: {lat:parseFloat(contact.lat), lng:parseFloat(contact.lng)},
                    map: map,
                    title: contact.title
                });

                //creates infobox, displays full address, gender and birthday
                var infowindow = new google.maps.InfoWindow({
                    content: contact.content.join(',')
                });

                //creates onclick listener to open the infobox when clicking on a marker
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            });

    }
}