<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 11/28/2017
 * Time: 14:46
 */


namespace ContactForm;

class Contact
{

    // all the fields for this object
    // same as custom post type contact_info meta-tags
    private $email, $adres_straat, $adres_num, $adres_num_postbus, $adres_postcode, $adres_gemeente, $geslacht, $geboortedatum, $latitude, $longitude;

    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getAdresStraat()
    {
        return $this->adres_straat;
    }

    /**
     * @param mixed $adres_straat
     */
    public function setAdresStraat($adres_straat)
    {
        $this->adres_straat = $adres_straat;
    }

    /**
     * @return mixed
     */
    public function getAdresNum()
    {
        return $this->adres_num;
    }

    /**
     * @param mixed $adres_num
     */
    public function setAdresNum($adres_num)
    {
        $this->adres_num = $adres_num;
    }

    /**
     * @return mixed
     */
    public function getAdresNumPostbus()
    {
        return $this->adres_num_postbus;
    }

    /**
     * @param mixed $adres_num_postbus
     */
    public function setAdresNumPostbus($adres_num_postbus)
    {
        $this->adres_num_postbus = $adres_num_postbus;
    }

    /**
     * @return mixed
     */
    public function getAdresPostcode()
    {
        return $this->adres_postcode;
    }

    /**
     * @param mixed $adres_postcode
     */
    public function setAdresPostcode($adres_postcode)
    {
        $this->adres_postcode = $adres_postcode;
    }

    /**
     * @return mixed
     */
    public function getAdresGemeente()
    {
        return $this->adres_gemeente;
    }

    /**
     * @param mixed $adres_gemeente
     */
    public function setAdresGemeente($adres_gemeente)
    {
        $this->adres_gemeente = $adres_gemeente;
    }

    /**
     * @return mixed
     */
    public function getGeslacht()
    {
        return $this->geslacht;
    }

    /**
     * @param mixed $geslacht
     */
    public function setGeslacht($geslacht)
    {
        $this->geslacht = $geslacht;
    }

    /**
     * @return mixed
     */
    public function getGeboortedatum()
    {
        return $this->geboortedatum;
    }

    /**
     * @param mixed $geboortedatum
     */
    public function setGeboortedatum($geboortedatum)
    {
        $this->geboortedatum = $geboortedatum;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }





    /** Inserts contact with validated info into the DB
     * @param $contact Contact instance of Contact class
     * @return boolean true on success, false on wp_error
     */
    public function add($contact):bool {

        // choose the correct custom post type, submit as published rather than draft, assign email field to post_title
        $contact_info = array(
            'post_type' => 'contact_info',
            'post_status' => 'publish',
            'post_title' => $contact->email
        );

        //get post ID of newly added post
        $postID = wp_insert_post($contact_info);

        if(is_wp_error($postID)) {
            return false;
        }

        //update meta-tags with the corresponding field value
        update_post_meta($postID, 'adres_straat', $contact->adres_straat);
        update_post_meta($postID, 'adres_num', $contact->adres_num);
        update_post_meta($postID, 'adres_num_postbus', $contact->adres_num_postbus);
        update_post_meta($postID, 'adres_postcode', $contact->adres_postcode);
        update_post_meta($postID, 'adres_gemeente', $contact->adres_gemeente);
        update_post_meta($postID, 'email', $contact->email);
        update_post_meta($postID, 'geslacht', $contact->geslacht);
        update_post_meta($postID, 'geboortedatum', $contact->geboortedatum);
        update_post_meta($postID, 'latitude', $contact->latitude ?? null);
        update_post_meta($postID, 'longitude', $contact->longitude ?? null);

        return true;
    }


    //returns false or an array of [latitude, longitude] for the current object's address

    /**
     * @param $contact Contact instance
     * @return array with geolocation data, empty array on failure
     */
    function getGeoLocation($contact):array {
        //prepare address string from content in this instance of Contact class
        $adres = $contact->adres_straat .' '. $this->adres_num .' '. $this->adres_postcode .' '. $this->adres_gemeente .', Belgium';

        //prepare address to insert safely into url
        $adres = urlencode($adres);

        //prepare api request url with address and api key (defined in googlemaps.php)
        $url = "https://maps.google.com/maps/api/geocode/json?address=$adres&key=".google_maps_API_key;


        //using wordpress function for remote get request to google geolocation API
        $response = wp_remote_get( $url );

        //http response code
        $response_code = wp_remote_retrieve_response_code($response);

        //response content
        $response_body = wp_remote_retrieve_body($response);

        if($response_code == 200) {

            //decoded response object, get the first entry of the results array in this object
            $result = json_decode($response_body)->results[0] ?? false;
            if($result !== false) {
                //read latitude, longitude and formatted address fields from $result
                $lat = $result->geometry->location->lat;
                $lng = $result->geometry->location->lng;
                $formatted_address = $result->formatted_address;

                //if lat and long and formatted address found in response
                if($lat && $lng && $formatted_address) {

                    //return lat and lng as an array
                    $geolocation =  ['latitude' => $lat, 'longitude' => $lng];
                    return $geolocation;
                }
            }


        }

        // return empty array if geolocation data was not found
        return [];
    }


    /**
     *  @return array of:
     *      headers: an array of all the meta-tags for custom post type contact_info
     *      data: an array of:
     *              n arrays of contact_info entries, data ordered in the same order as headers
     *
     * @param bool $include_key_names bool:
     *      defaults to false
     *      if true: makes elements of data array associative arrays with metatag names as keys, and contact_info entries as values
     */
    public static function export($include_key_names = false):array {

        //define arguments for WP query
        //only custom post type contact_info
        //only published posts
        $args = array(
            'post_type' => 'contact_info',
            'post_status' => array(
                'publish'
            )
        );

        //execute query and get array of posts
        $posts = new \WP_Query( $args );
        $posts = $posts->posts;

        //same as metatag names in custom post type contact_info
        $fields = array(
            'email',
            'adres_straat',
            'adres_num',
            'adres_num_postbus',
            'adres_postcode',
            'adres_gemeente',
            'geslacht',
            'geboortedatum',
            'latitude',
            'longitude'
        );


        //declare new array to return later
        $contactsArray = [];

        //loop through every element in queried posts
        foreach ( $posts as $post ) {

            //declare new array to hold all data about current contact
            $contactInfoArray = [];

            //loop through $fields as meta-tags for this contact, add values to $contactInfoArray
            foreach ( $fields as $field ) {
                $contactInfoArray[] = $post->$field;
            }

            //if this parameter is true, $contactInfoArray must be associative rather than numerical
            if($include_key_names) {
                $contactInfoArray = array_combine($fields, $contactInfoArray);
            }

            //add contact to $contactsArray
            $contactsArray[] = $contactInfoArray;
        }

        //return headers and data as an array
        return ['headers' => $fields, 'data' => $contactsArray];

    }


    /**
     * changes headers to CSV type, forces downloads, and exit() on download end
     */
    public static function export_to_CSV() {

        //changes the filename of exported CSV file every day
        $filename = 'contacts_data_'.date('d-m-Y').'.csv';

        /* Headers:
         *  forces download
         *  define application type as CSV
         *  tell server to not cache this resource
         *  download filename as attachment
         *  use character encoding: UTF-8
         */
        header("content-type: application/force-download");
        header('Content-Type: application/csv');
        header('Pragma: no-cache');
        header('Content-Disposition: attachment; filename=' . $filename);
        header("Content-Transfer-Encoding: UTF-8");


        //request array of all headers and data from all entries of custom post type Contact_info
        $contacts_array = Contact::export();

        //do not write to disk, but to php output directly
        $handle = fopen('php://output', 'w');

        //put headers in the CSV output file
        fputcsv($handle, $contacts_array['headers']);

        //put all entries under data in the CSV output file
        foreach($contacts_array['data'] as $entry) {
            fputcsv($handle, $entry);
        }

        //close output file and exit page
        fclose($handle);
        exit();
    }

}
?>