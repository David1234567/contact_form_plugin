<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 11/28/2017
 * Time: 15:24
 */


namespace ContactForm;




/**
 * Class Form is used to:
 *      build the contact form HTML
 *      run validation checks on form submissions
 *      display error messages to the user if one or multiple fields are invalid
 *      lets user resubmit failed form submissions without having to re-enter fields that were valid
 */
class Form
{

    /**
     *  builds the contact form HTML
     */
    public function build() {
        echo '
            <div>
                <!-- contact form, sends POST data to itself-->
                <form name="contactformulier" method="post">
                    <table>
                        <tr>
                            <th colspan="2">Jouw gegevens: </th>
                        </tr>
                        <tr>
                            <td><label for="adres_straat">Adres: </label></td>
                            <td>
                                <!-- adres values, only postbus is not required. On failed form submission, valid fields do not have to be re-entered. front-end validation with pattern regexes and maxlength property -->
                                <input type="text" name="adres_straat" value="' . ($_POST['adres_straat'] ?? null) . '" placeholder="straatnaam" pattern="[a-zA-Z- ]{1,25}" maxlength="25" id="adres_straat" required /><br />
                                <input type="text" name="adres_num" value="' . ($_POST['adres_num'] ?? null) . '" placeholder="huis nr." pattern="[0-9]{1,4}" maxlength="4" required />
                                <input type="text" name="adres_num_postbus" value="' . ($_POST['adres_num_postbus'] ?? null) . '" placeholder="postbus" pattern="[a-zA-Z0-9]{0,2}" maxlength="2" /><br />
                                <input type="text" name="adres_postcode" value="' . ($_POST['adres_postcode'] ?? null) . '" placeholder="postcode" pattern="[0-9]{4}" maxlength="4" required /><br />
                                <input type="text" name="adres_gemeente" value="' . ($_POST['adres_gemeente'] ?? null) . '" placeholder="gemeente" pattern="[a-zA-Z- ]{1,25}" maxlength="25" required />	
                            </td>
                        </tr>
                        <tr>
                            <td><label for="email">e-mail: </label></td>
                            <td>
                                <!-- email address, required, front end validation using input type email -->
                                <input type="email" name="email" value="' . ($_POST['email'] ?? null) . '" placeholder="e-mail adres" id="email" required />
                            </td>
                        </tr>
                        <tr>
                            <td><label>geslacht: </label></td>
                            <td>
                                <!-- gender, required, must be either of the provided options of input type radio with name of "geslacht" -->
                                <label for="geslacht_m"><input type="radio" name="geslacht" ' . (@$_POST['geslacht'] == 'geslacht_m' ? ' checked' : null) . ' value="geslacht_m" id="geslacht_m" required /> man</label>
                                <label for="geslacht_v"><input type="radio" name="geslacht" ' . (@$_POST['geslacht'] == 'geslacht_v' ? ' checked' : null) . ' value="geslacht_v" id="geslacht_v" required /> vrouw</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="geboortedatum">geboortedatum: </label></td>
                            <td>
                                <!-- date of birth, required, front end validation using pattern regex-->
                                <!-- uses input type text rather than input type date to prevent misrepresenting the correct date format -->
                                <input type="text" name="geboortedatum" pattern="[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}" placeholder="dd-mm-yyyy" value="' . ($_POST['geboortedatum'] ?? null) . '" id="geboortedatum" required />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <!-- submit button -->
                                <input type="submit" value="toevoegen" />
                            </td>
                        </tr>
                    </table>
                
                </form>
            </div>';
    }

    /**
     * @param $form_data array(String, String, String, String, String, String, String, String, DateTime)
     * @return bool  boolean return value indicates if form submission was successful
     *
     *  this function validates all user input and then adds an entry to the custom post type "contact_info"
     *  it also checks if the unique key (email address) already exists in the list of custom_post_type "contact_info"
     *  calls displayErrorMsg() on failure
     */
    public static function validate($form_data):array {
        //initialize to true, set to false if one or multiple checks fail
        $response= array(
            'valid' => true,
            'errors' =>array()
        );

            // _POST data are kept on form submission so the user doesn't need to re-enter every field
            // if a check fails, the POST data for this field is unset, and $valid will be false
            // as well as displaying a predefined error message for each specific failure point

            //straat can only consist of letters, spaces and dashes, up to 25 characters
            if (preg_match("/^[a-zA-Z- ]{1,25}$/", $form_data['adres_straat']) == false) {
                $response['errors'][] = Form::displayErrorMsg('adres_straat');
                $_POST['adres_straat'] = null;
                $response['valid'] = false;
            }

            //huis nr. can only consist of 1-4 digits
            if (preg_match("/^[0-9]{1,4}$/", $form_data['adres_num']) == false) {
                $response['errors'][] = Form::displayErrorMsg('adres_num');
                $_POST['adres_num'] = null;
                $response['valid'] = false;
            }

            //postbus can only consist of up to 2 alphanumerical characters
            if (preg_match("/^[a-zA-Z0-9]{0,2}$/", $form_data['adres_num_postbus']) == false) {
                $response['errors'][] = Form::displayErrorMsg('adres_num_postbus');
                $_POST['adres_num_postbus'] = null;
                $response['valid'] = false;
            }

            //postcode can only consist of exactly 4 digits
            if (preg_match("/^[0-9]{4}$/", $form_data['adres_postcode']) == false) {
                $response['errors'][] = Form::displayErrorMsg('adres_postcode');
                $_POST['adres_postcode'] = null;
                $response['valid'] = false;
            }

            //gemeente can only consist of letters, spaces and dashes, up to 25 characters
            if (preg_match("/^[a-zA-Z- ]{1,25}$/", $form_data['adres_gemeente']) == false) {
                $response['errors'][] = Form::displayErrorMsg('adres_gemeente');
                $_POST['adres_gemeente'] = null;
                $response['valid'] = false;
            }

            //email must use a valid syntax
            if (filter_var($form_data['email'], FILTER_VALIDATE_EMAIL) == false) {
                $response['errors'][] = Form::displayErrorMsg('email');
                $_POST['email'] = null;
                $response['valid'] = false;
            }

            //geslacht must be either geslacht_m or geslacht_v
            if ($form_data['geslacht'] != 'geslacht_m' && $form_data['geslacht'] != 'geslacht_v') {
                $response['errors'][] = Form::displayErrorMsg('geslacht');
                $response['valid'] = false;
            }


            /*
             *  $maxDate: creates current date (upper range for geboortedatum input, ie: today)
             *  $minDate: creates date for start of 20th century (lower range for geboortedatum input)
             *  checks geboortedatum user input against both ranges, and a formatted representation of itself, preventing out-of-bounds or invalid dates
             */
            $maxDate = \DateTime::createFromFormat("d-m-Y", date("d-m-Y"));
            $minDate = \DateTime::createFromFormat("Y", "1900");
                //check if date is out of bounds in either direction, also checks the date format in the section below
            if ($form_data['geboortedatum'] > $maxDate || $form_data['geboortedatum'] < $minDate ||
                    /*check if user inputted date is equal to its date object, in one of the following formats:
                     *  day-month-year
                     *  05-02-2017  leading zeroes for day and month
                     *  05-1-2017   leading zero for day
                     *  5-02-2017   leading zero for month
                     *  5-2-2017    no leading zeroes
                     *invert the boolean result of this section
                     *returns false (input is not invalid) when POST data matches one of the given formats
                     *returns true (input is invalid) when POST data matches none of the given formats
                     *
                     */
                !(  $form_data['geboortedatum']->format("d-m-Y") == $_POST['geboortedatum'] ||
                    $form_data['geboortedatum']->format("d-n-Y") == $_POST['geboortedatum'] ||
                    $form_data['geboortedatum']->format("j-m-Y") == $_POST['geboortedatum'] ||
                    $form_data['geboortedatum']->format("j-n-Y") == $_POST['geboortedatum'])
            ) {
                $response['errors'][] = Form::displayErrorMsg('geboortedatum');
                $_POST['geboortedatum'] = null;
                $response['valid'] = false;
            }
            // end of validation checks


            if ($response['valid']) {
                //query all existing entries of this custom post type with the same email address as the user form input
                $args = array(
                    //type must be custom post type 'contact_info'
                    'post_type' => 'contact_info',
                    //array of arrays of meta-tag and their respective value to search for
                    'meta_query' => array(
                        array(
                            'key' => 'email',
                            'value' => $form_data['email']
                        )
                    ),
                    //status of posts: only search fully published entries
                    'post_status' => array(
                        'publish'
                    )
                );
                //make WP_Query object using the above arguments
                $duplicateContacts = new \WP_Query($args);


                // display error message (duplicate email)
                if ($duplicateContacts->found_posts > 0) {
                    $response['errors'][] = Form::displayErrorMsg('duplicate_email');
                    $_POST['email'] = null;
                    $response['valid'] = false;
                }
            }

        return $response;
    }


    /*
     *  this function displays one or more predefined error messages for each invalid input field that was submitted by user
     */
    static function displayErrorMsg($field):String
    {
        $errorMsgArray = [
            'adres_straat' => 'straatnaam kan alleen uit a-z en - bestaan',
            'adres_num' => 'huisnummer kan alleen uit cijfers bestaan',
            'adres_num_postbus' => 'postbus nummer kan alleen uit letters bestaan',
            'adres_postcode' => 'postcode moet bestaan uit 4 cijfers',
            'adres_gemeente' => 'gemeente kan alleen uit a-z en - bestaan',
            'email' => 'email adres is niet geldig',
            'geslacht' => 'gelieve je geslacht aan te duiden',
            'geboortedatum' => 'kies een geldige geboortedatum',
            'duplicate_email' => 'contact info voor dit email adres bestaat al',
            'incomplete' => 'gelieve ieder verplicht veld in te vullen'
        ];
        return ($errorMsgArray[$field]) ? '<em>* ' . $errorMsgArray[$field] . ' </em><br />' : null;
    }
}